#!/usr/bin/env node
const path = require('path');
const cp = require('child_process');
const program = require('commander');
const execute = require('./lib/execute');
require('dotenv').config({ path: path.join(__dirname, '.env') });
require('dotenv').config({ path: path.join(process.cwd(), '.env') });

const DEFAULT_BRANCH = 'bot-update-gitlab-ci-yml';
const DEFAULT_INSTANCE = `https://gitlab.com/`;

let targetGroup;

program
  .version(require('./package.json').version)
  .usage('[options] <group>')
  .option('-n, --dry-run, --noop', 'Dry run: Does not create any branches / merge requests')
  .option('-f, --force', 'Actively creates branches and merge requests')
  .option('--branch-name', `Specify a branch name, defaults to "${DEFAULT_BRANCH}"`)
  .option('--author-name', 'Specify author name, defaults to your git default')
  .option('--author-mail', 'Specify author email, defaults to your git default')
  .option('--gitlab-api-url', `Defaults to: "${DEFAULT_INSTANCE}"`)
  .action(function (group) {
    targetGroup = group;
  })
  .parse(process.argv);

function bail(message){
  console.error(message);
  process.exit(1);
}

if(!targetGroup){
  bail(`Please provide a group in which we shall update the gitlab-ci.yml`)
}

if (!process.env.GITLAB_TOKEN) {
  bail(`Please provide a GitLab API token with the env variable GITLAB_TOKEN`)
}

let dryRun = true;

if(program.dryRun && program.force){
  bail('Please provide just one, --dry-run or --force')
} else if (program.force) {
  dryRun = false;
}

const branchName = program.branchName || DEFAULT_BRANCH;
const apiUrl = program.gitlabApiUrl || DEFAULT_INSTANCE;
let authorName;
let authorEmail;
try {
  authorName = program.authorName || cp.execSync('git config --get user.name').toString().replace(/[\r\n]+/, '');
  authorEmail = program.authorEmail || cp.execSync('git config --get user.email').toString().replace(/[\r\n]+/, '');
} catch (e) {
  bail('Could not retrieve author Credentials from git. Please make sure git is installed or you provide the author details as arguments.');
}

execute({branchName, authorEmail, authorName, apiUrl, targetGroup: encodeURI(targetGroup), dryRun, token: process.env.GITLAB_TOKEN});
