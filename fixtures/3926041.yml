image: registry.gitlab.com/gitlab-org/gitlab-build-images:ruby-2.3-golang-1.9-git-2.14

stages:
  - build
  - test
  - publish

variables:
  DOCKER_DRIVER: overlay
  CUSTOM_IMAGE_VERSION: ruby-2.3-golang-1.9-git-2.14

verify:
  stage: test
  script:
    - make verify

.ruby_template: &ruby_definition
  before_script:
    # Override gemfile config (for some reasong `config --delete` doesn't do it)
    - bundle config --local gemfile Gemfile
  cache:
    key: gems
    paths:
      - ruby/vendor/bundle

.build_template: &build_definition
  <<: *ruby_definition
  stage: build
  script:
    - make
    - _support/test-boot-time .

.assemble_template: &assemble_definition
  <<: *ruby_definition
  stage: build
  only:
    - tags
  script:
    # Just in case we start running CI builds on other architectures in future
    - go version
    - make binaries
  artifacts:
    paths:
    - _build/assembly/checksums.sha256.txt
    - _build/assembly/bin/
    name: "$CI_JOB_NAME"
    expire_in: 6 months

.test_template: &test_definition
  <<: *ruby_definition
  stage: test
  # Override the ruby_definition cache definition for pull
  cache:
    key: gems
    paths:
      - ruby/vendor/bundle
    policy: pull
  script:
    - go version
    - git version
    - make test

build:go1.10:
  <<: *build_definition
  image: registry.gitlab.com/gitlab-org/gitlab-build-images:ruby-2.3-golang-1.10-git-2.16

build:go1.9:
  <<: *build_definition
  image: registry.gitlab.com/gitlab-org/gitlab-build-images:ruby-2.3-golang-1.9-git-2.16

build:go1.8:
  <<: *build_definition
  image: registry.gitlab.com/gitlab-org/gitlab-build-images:ruby-2.3-golang-1.8.6-git-2.14

binaries_go1.10:
  <<: *assemble_definition
  image: registry.gitlab.com/gitlab-org/gitlab-build-images:ruby-2.3-golang-1.10-git-2.16

binaries_go1.9:
  <<: *assemble_definition
  image: registry.gitlab.com/gitlab-org/gitlab-build-images:ruby-2.3-golang-1.9-git-2.16

binaries_go1.8:
  <<: *assemble_definition
  image: registry.gitlab.com/gitlab-org/gitlab-build-images:ruby-2.3-golang-1.8.6-git-2.14

test:default:
  <<: *test_definition

test:go1.8-git2.14:
  <<: *test_definition
  image: registry.gitlab.com/gitlab-org/gitlab-build-images:ruby-2.3-golang-1.8-git-2.14

test:go1.9-git2.9:
  <<: *test_definition
  image: registry.gitlab.com/gitlab-org/gitlab-build-images:ruby-2.3-golang-1.9-git-2.9

test:go1.9-git2.14:
  <<: *test_definition
  image: registry.gitlab.com/gitlab-org/gitlab-build-images:ruby-2.3-golang-1.9-git-2.14

test:go1.10-git2.14:
  <<: *test_definition
  image: registry.gitlab.com/gitlab-org/gitlab-build-images:ruby-2.3-golang-1.10-git-2.14

test:go1.9-git2.16:
  <<: *test_definition
  image: registry.gitlab.com/gitlab-org/gitlab-build-images:ruby-2.3-golang-1.9-git-2.16

test:go1.10-git2.16:
  <<: *test_definition
  image: registry.gitlab.com/gitlab-org/gitlab-build-images:ruby-2.3-golang-1.10-git-2.16

cover:
  <<: *ruby_definition
  stage: test
  script:
    - make cover
  after_script:
    - bash <(curl -s https://codecov.io/bash) -t "${CODECOV_TOKEN}" -f _build/cover/all.merged -F unittests
  artifacts:
    paths:
      - _build/cover/all.html
    expire_in: 1 week

# Create a code quality report
codequality:
  image: docker:latest
  stage: test
  services:
    - docker:dind
  script:
    - docker run --env CODECLIMATE_CODE="$PWD" --volume "$PWD":/code --volume /var/run/docker.sock:/var/run/docker.sock --volume /tmp/cc:/tmp/cc dev.gitlab.org:5005/gitlab/gitlab-build-images:gitlab-codeclimate analyze -f json > codeclimate.json
  artifacts:
    paths: [codeclimate.json]
    expire_in: 1 week
  allow_failure: true

.build_image: &build_image
  stage: publish
  image: docker:latest
  services:
  - docker:dind

docker:
  <<: *build_image
  script:
    - mkdir -p _support/build-images/$CI_JOB_NAME/
    - cp ruby/Gemfile ruby/Gemfile.lock _support/build-images/$CI_JOB_NAME/
    - cp _support/build-images/versioned/Dockerfile _support/build-images/$CI_JOB_NAME/
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    - docker build -t $CI_REGISTRY_IMAGE/gitaly:latest --build-arg CUSTOM_IMAGE_VERSION=$CUSTOM_IMAGE_VERSION _support/build-images/$CI_JOB_NAME
    - docker push $CI_REGISTRY_IMAGE/gitaly:latest
  only:
    - master

docker-tag:
  <<: *build_image
  script:
    - mkdir -p _support/build-images/$CI_JOB_NAME/
    - cp ruby/Gemfile ruby/Gemfile.lock _support/build-images/$CI_JOB_NAME/
    - cp _support/build-images/versioned/Dockerfile _support/build-images/$CI_JOB_NAME/
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    - docker build -t $CI_REGISTRY_IMAGE/gitaly:$CI_COMMIT_TAG --build-arg CUSTOM_IMAGE_VERSION=$CUSTOM_IMAGE_VERSION _support/build-images/$CI_JOB_NAME
    - docker push $CI_REGISTRY_IMAGE/gitaly:$CI_COMMIT_TAG
  only:
    - tags@gitlab-org/gitaly

# Create a MR in GitLab-CE when releasing a Gitaly version
update-downstream-server-version:
  stage: publish
  only:
    - tags@gitlab-org/gitaly
  script:
    - ruby _support/update-downstream-server-version
  allow_failure: true

# Ensure that gitlab-git vendoring from gitlab-ce is functioning
test-vendor-gitaly-ruby:
  stage: test
  only:
   - schedules
  before_script:
    - DEBIAN_FRONTEND=noninteractive apt-get update -qq && apt-get -q -y install rsync
  script:
    - _support/vendor-gitlab-git master
    - make test

sast:
  image: registry.gitlab.com/gitlab-org/gl-sast:latest
  before_script: []
  stage: test
  variables:
    CONFIDENCE_LEVEL: 2
  script:
    - /app/bin/run ./ruby
    - mv ruby/gl-sast-report.json ./
  artifacts:
    paths:
    - gl-sast-report.json

