const fixReportSyntax = require('./fixReportSyntax');
const fs = require('fs');
const path = require('path');
const glob = require('glob');

const testFiles = glob.sync(path.join(__dirname, '..', 'fixtures', '*.yml'))

test.each(testFiles)('converts yml properly for %p', (file) => {

  const content = fs.readFileSync(file, 'utf8');

  expect(fixReportSyntax(content)).toMatchSnapshot();
});


// .thru(fixReportEntry('gl-sast-report.json', 'sast'))
//   .thru(fixReportEntry('gl-dast-report.json', 'dast'))
//   .thru(fixReportEntry('gl-code-quality-report.json', 'codequality'))
//   .thru(fixReportEntry('codeclimate.json', 'codequality'))
//   .thru(fixReportEntry('gl-license-management-report.json', 'license_management'))
//   .thru(fixReportEntry('gl-container-scanning-report.json', 'container_scanning'))
//   .thru(fixReportEntry('gl-sast-container-report.json', 'container_scanning'))
//   .thru(fixReportEntry('gl-dependency-scanning-report.json', 'dependency_scanning'))
//   .thru(fixReportEntry('performance.json', 'performance'))

test('should fix old sast reports', () => {

  expect(fixReportSyntax(`
  artifacts:
    paths: [gl-sast-report.json]
  `)).toEqual(`
  artifacts:
    paths: [gl-sast-report.json]
  `)
})
