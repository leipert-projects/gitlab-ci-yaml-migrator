const _ = require('lodash');
const YAMLParser = require('js-yaml');

const fixReportEntry = (artifactName, reportSection) => (yaml) => {
  const indentation = /^(\s+)artifacts:/m.exec(yaml)[1] || '  ';

  const searchForOneLineBracketSyntax = new RegExp(`^(\\s+)paths:[\\s\\[]*?(["']?${artifactName}["']?)[\\s\\]]`, "gm");
  const searchForHyphenSyntax = new RegExp(`^((\\s+?)paths:[\\s\\S]+?)^.*?(["']?${artifactName}["']?)\\s*`, "gm");

  var regex = /^(\s*?)artifacts:[\s\S]+?(?=(\r?\n\1\S|\r?\n\r?\n|\r?\n$))/gm;

  let result;
  let replacements = [];
  while (result = regex.exec(yaml)) {

    const match = result[0].split(/^\s+$/gm)[0];

    let replacement = match
      .replace(searchForOneLineBracketSyntax, `$1reports:\n$1${indentation}${reportSection}: $2`)
      .replace(searchForHyphenSyntax, `$2reports:\n$2${indentation}${reportSection}: $3\n$1`)

    try {
      const safeParsed = YAMLParser.safeLoad(replacement);
      if (safeParsed.artifacts && (!safeParsed.artifacts.paths || safeParsed.artifacts.paths.length === 0)) {
        replacement = replacement.replace(/^(\s+?)paths:\s+/m, '')
      }
    } catch (e) {
      console.warn(replacement);
    }


    replacements.push({
      start: result.index,
      end: result.index + match.length,
      search: match,
      replacement: replacement
    })
  }

  let resultS = yaml;

  for (let i = replacements.length; i--; i > 0) {
    const rep = replacements[i];

    resultS = resultS.substring(0, rep.start) + rep.replacement + resultS.substring(rep.end)
  }

  return resultS;
};

module.exports = function fixReportSyntax(yaml) {
  return _(yaml)
    .thru(fixReportEntry('gl-sast-report.json', 'sast'))
    .thru(fixReportEntry('gl-dast-report.json', 'dast'))
    .thru(fixReportEntry('gl-code-quality-report.json', 'codequality'))
    .thru(fixReportEntry('codeclimate.json', 'codequality'))
    .thru(fixReportEntry('gl-license-management-report.json', 'license_management'))
    .thru(fixReportEntry('gl-container-scanning-report.json', 'container_scanning'))
    .thru(fixReportEntry('gl-sast-container-report.json', 'container_scanning'))
    .thru(fixReportEntry('gl-dependency-scanning-report.json', 'dependency_scanning'))
    .thru(fixReportEntry('performance.json', 'performance'))
    .value();
};
