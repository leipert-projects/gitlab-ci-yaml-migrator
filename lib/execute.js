const _ = require('lodash');
const fs = require('fs');
const jsYaml = require('js-yaml');
const path = require("path");
const GitLabAPIWrapper = require('gitlab/dist/es5').default;

function getProjectYAML(GitLab, project) {
  return GitLab.RepositoryFiles.showRaw(project.id, '.gitlab-ci.yml', project['default_branch'])
    .catch((e) => {
      if (e.statusCode === 404) {
        return null;
      }
      console.warn(`Error retrieving .gitlab.yml for ${project.web_url}: ${e.message}`);
      return null;
    })
    .then((yaml) => {
      let parsed = null;
      try {
        parsed = jsYaml.safeLoad(yaml)
      } catch (e) {
        console.warn(`Error parsing .gitlab-ci.yml for ${project.web_url}: ${e.message}`)
      }

      return { project, yaml, parsed };
    });
}

function searchForApplicableProjects(GitLab, targetGroup) {
  return GitLab.GroupProjects.all(targetGroup)
    .then(groups => {
      return Promise.all(groups.map(project => getProjectYAML(GitLab, project)))
    })
}

const blacklist = [
  'gl-sast-report.json',
  'gl-dast-report.json',
  'gl-code-quality-report.json',
  'codeclimate.json',
  'gl-license-management-report.json',
  'gl-container-scanning-report.json',
  'gl-sast-container-report.json',
  'gl-dependency-scanning-report.json',
  'performance.json',
];

function hasDeprecatedReportsSyntax(project, parsed) {
  return _.some(parsed, (value) => {
    return value.artifacts && value.artifacts.paths && (blacklist.includes(value.artifacts.paths) || _.intersection(blacklist, value.artifacts.paths).length)
  })
}

function checkYAML(project, parsed, yaml) {

  if (parsed === null) {
    console.warn(`${project.web_url}: has no .gitlab-ci.yml`);
    return;
  }

  if(hasDeprecatedReportsSyntax(project, parsed, yaml)){
   console.warn(`${project.web_url}: needs an update`)
  } 

}

module.exports = function execute({ branchName, authorEmail, authorName, apiUrl, targetGroup, dryRun, token }) {
  if (dryRun) {
    console.warn(`
  ###########################
  DRY RUN
  ###########################
  `)
  }

  console.warn(`
  Branch name: ${branchName}
  Commit Author: "${authorName}" <${authorEmail}>
  GitLab Instance: ${apiUrl}
  Group: ${targetGroup}
`);

  const api = new GitLabAPIWrapper({
    url: apiUrl,
    token: token
  });

  searchForApplicableProjects(api, targetGroup)
    .then((results) => {
      return Promise.all(results.map(({ project, yaml, parsed }) => {
        if(dryRun){
          return checkYAML(project, parsed, yaml);
        }
      }));
    });

};


